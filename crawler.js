var request = require('request');
var cheerio = require('cheerio');
var URL = require('url-parse');
var fs = require('fs');

var START_URL = "https://wiprodigital.com";
var MAX_PAGES_TO_VISIT = 20;

var pagesVisited = {};
var numPagesVisited = 0;
var pagesToVisit = [];
var url = new URL(START_URL);
var baseUrl = url.protocol + "//" + url.hostname;

pagesToVisit.push(START_URL);
crawl();

function crawl() {
    if(numPagesVisited >= MAX_PAGES_TO_VISIT) {
        console.log("Reached max limit of number of pages to visit.");
        return;
    }
    var nextPage = pagesToVisit.pop();
    if (nextPage in pagesVisited) {
        // We've already visited this page, so repeat the crawl
        crawl();
    } else {
        // New page we haven't visited
        visitPage(nextPage, crawl);
    }
}

/**
 * Fetch url page content
 * @param url
 * @param callback
 */
function visitPage(url, callback) {
    // Add page to our set
    pagesVisited[url] = true;
    numPagesVisited++;

    // Make the request
    console.log("Visiting page " + url);
    request(url, function(error, response, body) {
        // Check status code (200 is HTTP OK)
        console.log("Status code: " + response.statusCode);
        if(response.statusCode !== 200) {
            callback();
            return;
        }
        // Parse the document body
        var $ = cheerio.load(body);

        collectPageLinks($);
        // In this short program, our callback is just calling crawl()
       callback();
    });
}

/**
 * Extract all the links on the web page and store in text file
 * @param $
 */
function collectPageLinks($) {
    var pageLinks = $("a[href],img[src]");
    console.log("Found " + pageLinks.length + " page links on page");
    pageLinks.each(function() {
        const url = $(this).attr('href') || $(this).attr('src');
        if (url.indexOf('http')===0) {

            if (url.indexOf(baseUrl) !== -1) {
                pagesToVisit.push(url);
            }

            fs.appendFileSync('pageLinks.txt', url + '\n');

        }
    });
}